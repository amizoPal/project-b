package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/demo")
public class DemoController {
    @GetMapping("/hello")
    public String hello(){
        return "hello";
    }

    @GetMapping("/goodbye")
    public String goodbye(){
        return "goodbye!";
    }
}
