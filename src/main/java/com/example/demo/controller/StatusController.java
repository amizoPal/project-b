package com.example.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/status")
public class StatusController {
    @GetMapping("/dev")
    public String dev(){
        return "dev";
    }

    @GetMapping("/func1")
    public String func1(){
        return "Funcionality 1";
    }

    @GetMapping("/func2")
    public String func2(){
        return "Funcionality 2";
    }
}
